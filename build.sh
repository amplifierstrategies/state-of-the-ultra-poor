#!/bin/sh

echo '### Fetching content ###'
jekyll contentful
echo '### Building site ###'
JEKYLL_ENV=production jekyll build 
echo '### DONE ###'
