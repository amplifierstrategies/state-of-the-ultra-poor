module.exports = {
  options: {
    mergeIntoShorthands: false,
    roundingPrecision: -1  
  },
  target: {
    files: {
      'css/app.min.css': [
      	'assets/css/libs/bootstrap.min.css',
       	'assets/css/libs/animate.min.css',
       	'assets/slick/slick.css',
       	'assets/slick/slick-theme.css',
       	'assets/css/libs/font-awesome.min.css']
    }
  }
}
