module.exports = {
  options: {
    prefix : 'icon-', // This will prefix each <g> ID
  },
  default : {
      files: {
        '_includes/icon-sprite.svg': ['_src/svg/*.svg'],
      }
    }
 }