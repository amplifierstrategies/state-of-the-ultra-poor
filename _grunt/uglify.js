module.exports = {
    javascript_files: {
      files: {
        'assets/js/libs.min.js': [
          'assets/js/lib/jquery.js', 
          'assets/js/lib/bootstrap.min.js', 
          'assets/js/lib/popper.min.js', 
          'assets/js/lib/jquery.backstretch.min.js', 
          'assets/js/lib/wow.min.js', 
          'assets/slick/slick.min.js', 
          'assets/js/lib/datatables.min.js'
          ],
        'assets/js/app.min.js': [ 'assets/js/lib/custom.js']
      }
    }
}