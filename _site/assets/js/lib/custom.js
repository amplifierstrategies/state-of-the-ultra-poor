
 /* jQuery Pre loader
  -----------------------------------------------*/
$(window).load(function(){
    $('.preloader').fadeOut(1000); // set duration in brackets    
});




$(document).ready(function() {

  /* Map
  -----------------------------------------------*/
    if ($('#world-map')[0]) {
        $('#world-map').vectorMap({
          map: 'world_mill',
          series: {
            regions: [{
              values: mapData,
              scale: ['#ffffff', '#eaad2e'],
              normalizeFunction: 'polynomial'
            }]
          },
          onRegionTipShow: function(e, el, code){
            el.html(el.html());
          }
        });  
    }


  /* Full Screen Menu
  -----------------------------------------------*/
  $(".button a").click(function(){
      $(".overlay").fadeToggle(200);
     $(this).toggleClass('btn-open').toggleClass('btn-close');
  });

  $('.overlay').on('click', function(){
      $(".overlay").fadeToggle(200);   
      $(".button a").toggleClass('btn-open').toggleClass('btn-close');
      open = false;
  });


  /* wow
  -------------------------------*/
  new WOW({ mobile: false }).init();


  /* Quotes slideshow
  -------------------------------*/
  if ($('.quotes-container')[0]) {
    $('.quotes-container').slick({
      slidesToScroll: 1,
      autoplay: false,
      fade: true,
      nextArrow: "<img class='a-right control-c next slick-next' src='/assets/images/arrow.png'>",
      prevArrow: "<img class='a-left control-c prev slick-prev' src='/assets/images/arrow-left.png'>"
    });
  }


  /* Blog slideshow
  -------------------------------*/
  if ($('.blogSlideshow')[0]) {
    $('.blogSlideshow').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        nextArrow: "<img class='a-right control-c next slick-next' src='/assets/images/arrow-right-dark.png'>",
        prevArrow: "<img class='a-left control-c prev slick-prev' src='/assets/images/arrow-left-dark.png'>",
         responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
    });
  }


    /* Tooltips
    -------------------------------*/
    $('.blog-post-content a').attr('target', '_blank');

  /* Tooltips
  -------------------------------*/
  $('[data-toggle="tooltip"]').tooltip({
    container: 'body',
    placement: 'bottom'
  })


  /* DataTables - Index Ranking
  -------------------------------*/
  if ($('#datatable')[0]) {
    $('#datatable').DataTable( {
        "ajax": "/assets/data/ultra-poor-data.json",
        "paging":   false,
        "info":     false,
        "searching":     false,
        "columns": [
            { "data": "Score" },
            { "data": "Country" },
            { "data": "Soc Assist" },
            { "data": "Annual Growth" },
            { "data": "Governance" },
            { "data": "Safety" },
            { "data": "Social K" },
            { "data": "Intensity of Poverty" },
            { "data": "Gender Inequality" },
            { "data": "Climate Risk" },
            { "data": "GDP" },
        ],
        "initComplete": function() {
            var triggers = {
                "country": {
                    "terribleTrigger": 28.6,
                    "lowTrigger": 39.4,
                    "mediumTrigger": 50.2,
                    "goodTrigger": 61.1
                },
                "socAssist": {
                    "terribleTrigger": 0.0,
                    "lowTrigger": 22.9,
                    "mediumTrigger": 45.9,
                    "goodTrigger": 68.8
                },
                "annualGrowth": {
                    "terribleTrigger": 0.0,
                    "lowTrigger": 25,
                    "mediumTrigger": 50,
                    "goodTrigger": 75
                },
                "governance": {
                    "terribleTrigger": 21,
                    "lowTrigger": 31.3,
                    "mediumTrigger": 41.5,
                    "goodTrigger": 51.8
                },
                "safety": {
                    "terribleTrigger": 33,
                    "lowTrigger": 43,
                    "mediumTrigger": 53,
                    "goodTrigger": 63
                },
                "socialK": {
                    "terribleTrigger": 35,
                    "lowTrigger": 43.3,
                    "mediumTrigger": 51.5,
                    "goodTrigger": 59.8
                },
                "intensityOfPoverty": {
                    "terribleTrigger": 31.0,
                    "lowTrigger": 48.3,
                    "mediumTrigger": 65.5,
                    "goodTrigger": 82.8
                },
                "womenInequality": {
                    "terribleTrigger": 32.6,
                    "lowTrigger": 46.0,
                    "mediumTrigger": 59.4,
                    "goodTrigger": 72.7
                }, 
                "climateRisk": {
                    "terribleTrigger": 15,
                    "lowTrigger": 35.3,
                    "mediumTrigger": 55.6,
                    "goodTrigger": 75.9
                },
                "gdp": {
                    "terribleTrigger": 0.0,
                    "lowTrigger": 25,
                    "mediumTrigger": 50,
                    "goodTrigger": 75
                },
            }

        // Score Column
        evaluateScoreAndColorCell(1, 
            triggers.country.goodTrigger, 
            triggers.country.mediumTrigger, 
            triggers.country.lowTrigger, 
            triggers.country.terribleTrigger
        );

        // Country Column
        prependCountryFlag(2);

        // socAssist Column
        evaluateScoreAndColorCell(3, 
            triggers.socAssist.goodTrigger, 
            triggers.socAssist.mediumTrigger, 
            triggers.socAssist.lowTrigger, 
            triggers.socAssist.terribleTrigger
        );

        evaluateScoreAndColorCell(4, 
            triggers.annualGrowth.goodTrigger, 
            triggers.annualGrowth.mediumTrigger, 
            triggers.annualGrowth.lowTrigger, 
            triggers.annualGrowth.terribleTrigger
        );

        evaluateScoreAndColorCell(5, 
            triggers.governance.goodTrigger, 
            triggers.governance.mediumTrigger, 
            triggers.governance.lowTrigger, 
            triggers.governance.terribleTrigger
        );

        evaluateScoreAndColorCell(6, 
            triggers.safety.goodTrigger, 
            triggers.safety.mediumTrigger, 
            triggers.safety.lowTrigger, 
            triggers.safety.terribleTrigger
        );

        evaluateScoreAndColorCell(7, 
            triggers.socialK.goodTrigger, 
            triggers.socialK.mediumTrigger, 
            triggers.socialK.lowTrigger, 
            triggers.socialK.terribleTrigger
        );

        evaluateScoreAndColorCell(8, 
            triggers.intensityOfPoverty.goodTrigger, 
            triggers.intensityOfPoverty.mediumTrigger, 
            triggers.intensityOfPoverty.lowTrigger, 
            triggers.intensityOfPoverty.terribleTrigger
        );

        evaluateScoreAndColorCell(9, 
            triggers.womenInequality.goodTrigger, 
            triggers.womenInequality.mediumTrigger, 
            triggers.womenInequality.lowTrigger, 
            triggers.womenInequality.terribleTrigger
        );
        
        evaluateScoreAndColorCell(10, 
            triggers.climateRisk.goodTrigger, 
            triggers.climateRisk.mediumTrigger, 
            triggers.climateRisk.lowTrigger, 
            triggers.climateRisk.terribleTrigger
        );

        evaluateScoreAndColorCell(11, 
            triggers.gdp.goodTrigger, 
            triggers.gdp.mediumTrigger, 
            triggers.gdp.lowTrigger, 
            triggers.gdp.terribleTrigger
        );

        }
    } );
  }

  function prependCountryFlag(columnNumber) {
    $('#datatable tbody tr').find('td:nth-of-type(' + columnNumber + ')').each (function() {
        var currentCountry = $(this).html();
        var currentCountryCode = countryCodes[0][currentCountry];
        if (currentCountryCode) {
            $(this).prepend('<span class="flag-icon flag-icon-' + currentCountryCode.toLowerCase() + '"></span>');
        }
    });
  } 

  function evaluateScoreAndColorCell(columnNumber, goodTriger, mediumTrigger, lowTrigger, terribleTrigger) {
    // Process the desired column
    $('#datatable tbody tr').find('td:nth-of-type(' + columnNumber + ')').each (function() {
        var currentValue = $(this).html();
        var evaluation = '';
        if (!Number.isNaN(parseInt(currentValue))) {
          switch(true) {
            case (currentValue >= terribleTrigger && currentValue <= lowTrigger):
                evaluation = 'terrible';
              break;
            case (currentValue >= lowTrigger && currentValue <= mediumTrigger):
                evaluation = 'low';
              break;
            case (currentValue >= mediumTrigger && currentValue <= goodTriger):
                evaluation = 'medium';
              break;
            case (currentValue >= goodTriger):
                evaluation = 'high';
              break;
            default:
            break;
          }
         $(this).addClass(evaluation);
        }
    });
  }


  var countryCodes = [
  {
    "Afghanistan": "AF",
    "Aland Islands": "AX",
    "Albania": "AL",
    "Algeria": "DZ",
    "American Samoa": "AS",
    "Andorra": "AD",
    "Angola": "AO",
    "Anguilla": "AI",
    "Antarctica": "AQ",
    "Antigua and Barbuda": "AG",
    "Argentina": "AR",
    "Armenia": "AM",
    "Aruba": "AW",
    "Australia": "AU",
    "Austria": "AT",
    "Azerbaijan": "AZ",
    "Bahamas": "BS",
    "Bahrain": "BH",
    "Bangladesh": "BD",
    "Barbados": "BB",
    "Belarus": "BY",
    "Belgium": "BE",
    "Belize": "BZ",
    "Benin": "BJ",
    "Bermuda": "BM",
    "Bhutan": "BT",
    "Bolivia": "BO",
    "Bonaire, Sint Eustatius and Saba": "BQ",
    "Bosnia and Herzegovina": "BA",
    "Botswana": "BW",
    "Bouvet Island": "BV",
    "Brazil": "BR",
    "British Indian Ocean Territory": "IO",
    "Brunei Darussalam": "BN",
    "Bulgaria": "BG",
    "Burkina Faso": "BF",
    "Burundi": "BI",
    "Cambodia": "KH",
    "Cameroon": "CM",
    "Canada": "CA",
    "Cape Verde": "CV",
    "Cayman Islands": "KY",
    "Central African Republic": "CF",
    "Chad": "TD",
    "Chile": "CL",
    "China": "CN",
    "Christmas Island": "CX",
    "Cocos (Keeling) Islands": "CC",
    "Colombia": "CO",
    "Comoros": "KM",
    "Congo": "CG",
    "Congo DRC": "CD",
    "Cook Islands": "CK",
    "Costa Rica": "CR",
    "Cote d Ivoire": "CI",
    "Croatia": "HR",
    "Cuba": "CU",
    "CuraÃ§ao": "CW",
    "Cyprus": "CY",
    "Czech Republic": "CZ",
    "Denmark": "DK",
    "Djibouti": "DJ",
    "Dominica": "DM",
    "Dominican Republic": "DO",
    "Ecuador": "EC",
    "Egypt": "EG",
    "El Salvador": "SV",
    "Equatorial Guinea": "GQ",
    "Eritrea": "ER",
    "Estonia": "EE",
    "Ethiopia": "ET",
    "Falkland Islands (Malvinas)": "FK",
    "Faroe Islands": "FO",
    "Fiji": "FJ",
    "Finland": "FI",
    "France": "FR",
    "French Guiana": "GF",
    "French Polynesia": "PF",
    "French Southern Territories": "TF",
    "Gabon": "GA",
    "Gambia": "GM",
    "Georgia": "GE",
    "Germany": "DE",
    "Ghana": "GH",
    "Gibraltar": "GI",
    "Greece": "GR",
    "Greenland": "GL",
    "Grenada": "GD",
    "Guadeloupe": "GP",
    "Guam": "GU",
    "Guatemala": "GT",
    "Guernsey": "GG",
    "Guinea": "GN",
    "Guinea-Bissau": "GW",
    "Guyana": "GY",
    "Haiti": "HT",
    "Heard Island and McDonald Islands": "HM",
    "Holy See (Vatican City State)": "VA",
    "Honduras": "HN",
    "Hong Kong": "HK",
    "Hungary": "HU",
    "Iceland": "IS",
    "India": "IN",
    "Indonesia": "ID",
    "Iran, Islamic Republic of": "IR",
    "Iraq": "IQ",
    "Ireland": "IE",
    "Isle of Man": "IM",
    "Israel": "IL",
    "Italy": "IT",
    "Jamaica": "JM",
    "Japan": "JP",
    "Jersey": "JE",
    "Jordan": "JO",
    "Kazakhstan": "KZ",
    "Kenya": "KE",
    "Kiribati": "KI",
    "Korea, Democratic People's Republic of": "KP",
    "Korea, Republic of": "KR",
    "Kuwait": "KW",
    "Kyrgyzstan": "KG",
    "Lao": "LA",
    "Latvia": "LV",
    "Lebanon": "LB",
    "Lesotho": "LS",
    "Liberia": "LR",
    "Libya": "LY",
    "Liechtenstein": "LI",
    "Lithuania": "LT",
    "Luxembourg": "LU",
    "Macao": "MO",
    "Macedonia, the Former Yugoslav Republic of": "MK",
    "Madagascar": "MG",
    "Malawi": "MW",
    "Malaysia": "MY",
    "Maldives": "MV",
    "Mali": "ML",
    "Malta": "MT",
    "Marshall Islands": "MH",
    "Martinique": "MQ",
    "Mauritania": "MR",
    "Mauritius": "MU",
    "Mayotte": "YT",
    "Mexico": "MX",
    "Micronesia, Federated States of": "FM",
    "Moldova, Republic of": "MD",
    "Monaco": "MC",
    "Mongolia": "MN",
    "Montenegro": "ME",
    "Montserrat": "MS",
    "Morocco": "MA",
    "Mozambique": "MZ",
    "Myanmar": "MM",
    "Namibia": "NA",
    "Nauru": "NR",
    "Nepal": "NP",
    "Netherlands": "NL",
    "New Caledonia": "NC",
    "New Zealand": "NZ",
    "Nicaragua": "NI",
    "Niger": "NE",
    "Nigeria": "NG",
    "Niue": "NU",
    "Norfolk Island": "NF",
    "Northern Mariana Islands": "MP",
    "Norway": "NO",
    "Oman": "OM",
    "Pakistan": "PK",
    "Palau": "PW",
    "Palestine, State of": "PS",
    "Panama": "PA",
    "Papua New Guinea": "PG",
    "Paraguay": "PY",
    "Peru": "PE",
    "Philippines": "PH",
    "Pitcairn": "PN",
    "Poland": "PL",
    "Portugal": "PT",
    "Puerto Rico": "PR",
    "Qatar": "QA",
    "RÃ©union": "RE",
    "Romania": "RO",
    "Russian Federation": "RU",
    "Rwanda": "RW",
    "Saint BarthÃ©lemy": "BL",
    "Saint Helena, Ascension and Tristan da Cunha": "SH",
    "Saint Kitts and Nevis": "KN",
    "Saint Lucia": "LC",
    "Saint Martin (French part)": "MF",
    "Saint Pierre and Miquelon": "PM",
    "Saint Vincent and the Grenadines": "VC",
    "Samoa": "WS",
    "San Marino": "SM",
    "Sao Tome and Principe": "ST",
    "Saudi Arabia": "SA",
    "Senegal": "SN",
    "Serbia": "RS",
    "Seychelles": "SC",
    "Sierra Leone": "SL",
    "Singapore": "SG",
    "Sint Maarten (Dutch part)": "SX",
    "Slovakia": "SK",
    "Slovenia": "SI",
    "Solomon Islands": "SB",
    "Somalia": "SO",
    "South Africa": "ZA",
    "South Georgia and the South Sandwich Islands": "GS",
    "South Sudan": "SS",
    "Spain": "ES",
    "Sri Lanka": "LK",
    "Sudan": "SD",
    "Suriname": "SR",
    "Svalbard and Jan Mayen": "SJ",
    "Swaziland": "SZ",
    "Sweden": "SE",
    "Switzerland": "CH",
    "Syrian Arab Republic": "SY",
    "Taiwan, Province of China": "TW",
    "Tajikistan": "TJ",
    "Tanzania": "TZ",
    "Thailand": "TH",
    "Timor-Leste": "TL",
    "Togo": "TG",
    "Tokelau": "TK",
    "Tonga": "TO",
    "Trinidad and Tobago": "TT",
    "Tunisia": "TN",
    "Turkey": "TR",
    "Turkmenistan": "TM",
    "Turks and Caicos Islands": "TC",
    "Tuvalu": "TV",
    "Uganda": "UG",
    "Ukraine": "UA",
    "United Arab Emirates": "AE",
    "United Kingdom": "GB",
    "United States": "US",
    "United States Minor Outlying Islands": "UM",
    "Uruguay": "UY",
    "Uzbekistan": "UZ",
    "Vanuatu": "VU",
    "Venezuela, Bolivarian Republic of": "VE",
    "Viet Nam": "VN",
    "Virgin Islands, British": "VG",
    "Virgin Islands, U.S.": "VI",
    "Wallis and Futuna": "WF",
    "Western Sahara": "EH",
    "Yemen": "YE",
    "Zambia": "ZM",
    "Zimbabwe": "ZW"
  }
]

}); 



